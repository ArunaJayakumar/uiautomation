package com.test;

import java.net.MalformedURLException;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.Collections;
import java.util.List;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.FileUtils;
import org.apache.poi.xdgf.usermodel.section.GeometrySection;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.google.common.base.Function;

public class TC002_ConceptSearch extends ProjectWrappers{
	 public  TC002_ConceptSearch() throws MalformedURLException {
		// TODO Auto-generated constructor stub
	}

	
	@BeforeClass
	public void beforeClass() {
	
		testName = "TC002_ConceptsSearch";
		description="To verify Concepts search";
		author="Aruna";
		category="Smoke";
			}
	
	public WebDriver driver=getDriver();
	int i=0;
    
   
	
	@Test(groups= {"SmokeTest"})
	public void AudienceTestFilter() throws InterruptedException ,IOException{
		
		Properties prop=new Properties();
      	BufferedReader reader=new BufferedReader(new FileReader("./Data/login.properties"));
    	prop.load(reader);  
    String env=prop.getProperty("env");
    String user=prop.getProperty("username");
    String pwd=prop.getProperty("password");
	System.out.println("Starting test: "+testName);
		invokeApp(env, true);
		Thread.sleep(3000);
		try {
    	driver.findElement(By.xpath("(//span[text()='Continue with Google'])[2]")).click();
    	enterByXpath("//input[@name='identifier']",user,"Username");
    	Thread.sleep(2000);
    	driver.findElement(By.xpath("//div[@id='identifierNext']")).click();
    	Thread.sleep(3000);
    	driver.findElement(By.xpath("//input[@name='password']")).sendKeys(pwd);
    	driver.findElement(By.xpath("//div[@id='passwordNext']")).click();
    	reportStep("Pass","Cadenz is launched successfully");
    	}
    	catch(Exception e) {
    		reportStep("Fail","Not able to launch Cadenz url");
    		takeSnap(i++);
    	}
    	
    	Thread.sleep(5000);
    	Actions action=new Actions(driver);
    	
    	WebElement filter=driver.findElement(By.xpath("//div[@id='searchSwitch']"));
    	action.moveToElement(filter).build().perform();
   	Thread.sleep(3000);
    	driver.findElement(By.xpath("//span[text()='Concepts']")).click();
    	driver.findElement(By.xpath("//div[text()='Start Typing']")).click();
    	driver.findElement(By.xpath("//div[text()='Government']")).click();
    	driver.findElement(By.xpath("(//span[@class='MuiButton-label'])[1]")).click();
    	
    	//sendKeys("High Customer Value");
   	
    	try {
        	FluentWait<WebDriver> wait = new FluentWait<WebDriver>(driver);
    		wait.pollingEvery(5,  TimeUnit.SECONDS);
    		wait.withTimeout(5, TimeUnit.MINUTES);
    		wait.ignoring(NoSuchElementException.class); //make sure that this exception is ignored
    		Function<WebDriver, WebElement> function = new Function<WebDriver, WebElement>()
    				{
    					public WebElement apply(WebDriver arg0) {
    						System.out.println("Checking for the element!!");
    						WebElement element = arg0.findElement(By.xpath("//div[@class='ant-tabs-tab-active ant-tabs-tab']"));
    						if(element != null)
    						{
    							System.out.println("Target element found");
    							element.click();
    							 
    						}
    						return element;
    					}
    				};

    		wait.until(function);
    		 reportStep("Pass", "The Search results are ready to the user");
        	}
    	
        	catch(Exception e) {
        		reportStep("Fail","Not able Search by Concept");
        		takeSnap(i++);
        	}

    	takeSnap(i++);
    }
  
	public void takeSnap(int i) {
		 try {
			File tmp= ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
			 File dest= new File("./screenshots/"+testName+"_"+i+".png");
			 FileUtils.copyFile(tmp, dest);
		} catch (WebDriverException e) {
		System.err.println("Error while taking screenshot");
		
		} catch (IOException e) {
		System.err.println("Error while creating/copying the image file");
		}
			i++;
		}

}
