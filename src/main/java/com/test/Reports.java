package com.test;

import java.util.Locale;

import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;

public class Reports {
	
	public static ExtentReports report;
	public ExtentTest test;
	
	
	public void startReport() {
		report=new ExtentReports("./testreports/TestReport.html",false);	
	//	Locale.setDefault(Locale.ENGLISH);
	}
	
	public void startTest(String testName,  String description) {
		//Locale.setDefault(Locale.ENGLISH);
		test=report.startTest(testName, description);
	}
	
	public void reportStep(String status, String desc) {
		
		if(status.equalsIgnoreCase("pass")) {
		test.log(LogStatus.PASS, desc);
		}
		else if(status.equalsIgnoreCase("fail")) {
			test.log(LogStatus.FAIL, desc);
		}
	}
	
	public void endTest() {
		report.endTest(test);
	}
	
	public void endReport() {
		report.flush();
	}

}
