package com.test;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.net.MalformedURLException;
import java.util.Properties;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class TC004_Profiles extends ProjectWrappers{
	
	public  TC004_Profiles() throws MalformedURLException {
		// TODO Auto-generated constructor stub
	}
	
	@BeforeClass
	public void beforeClass() {
	
		testName = "TC004_Profiles";
		description="To verify Profiles Filter search";
		author="Aruna";
		category="Smoke";
			}
	
	public WebDriver driver;
	//@Parameters({ "environment" })
	@Test(groups= {"SmokeTest"})
	public void profileTest() throws InterruptedException, IOException {
		WebDriver driver=getDriver();
		int i=0;
		
		Properties prop=new Properties();
      	BufferedReader reader=new BufferedReader(new FileReader("./Data/login.properties"));
    	prop.load(reader);  
    String env=prop.getProperty("env");
    String user=prop.getProperty("username");
    String pwd=prop.getProperty("password");
	System.out.println("Starting test: "+testName);
		invokeApp("dev", true);

		    	Thread.sleep(3000);
		    	driver.findElement(By.xpath("(//span[text()='Continue with Google'])[2]")).click();
		    	Thread.sleep(3000);
		    	//driver.findElement(By.xpath("//input[@name='identifier']")).sendKeys("zpgs@globe.com.ph");
		    	driver.findElement(By.xpath("//input[@name='identifier']")).sendKeys(user);
		    	driver.findElement(By.xpath("//div[@id='identifierNext']")).click();
		    	//driver.findElement(By.xpath("(//div[@class='VfPpkd-RLmnJb'])[1]")).click();
		    	Thread.sleep(3000);
		    	
		    	driver.findElement(By.xpath("//input[@name='password']")).sendKeys(pwd);
		    	driver.findElement(By.xpath("//div[@id='passwordNext']")).click();
		    	Thread.sleep(10000);
		    	//driver.findElement(By.xpath("(//span[@class='RveJvd snByac'])[1]")).click();
		    	reportStep("Pass","Cadenz is launched successfully");
		/*/  	}
		    	catch(Exception e) {
		    		reportStep("Fail","Not able to launch Cadenz url");
		    		takeSnap(i++);
		    	}*/

		    	
		    	Thread.sleep(8000);
		    //	takeSnap(i++);
		 //   	driver.findElement(By.xpath("(//div[@class='ant-select-selection__rendered'])[1]")).click();
		
		try {
		    	WebElement ele=	driver.findElement(By.xpath("(//i[@class='anticon anticon-user'])[1]"));
		    	JavascriptExecutor exec=(JavascriptExecutor)driver;
		    	exec.executeScript("arguments[0].click();", ele);
		      	
		    	Thread.sleep(3000);
		    	reportStep("Pass","Profiles is launched successfully");
		}
    	catch(Exception e) {
    		reportStep("Fail","Not able to open Profiles");
    	//	takeSnap(i++);
    	}

		    	driver.findElement(By.xpath("//input[@id='customized_form_controls_searchInput']")).sendKeys("789654123");
		    	driver.findElement(By.xpath("//i[@class='anticon anticon-search ant-input-search-icon']")).click();
		    		}
	

	public void takeSnap(int i) {
		 try {
			File tmp= ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
			 File dest= new File("./screenshots/Profiles"+i+".png");
			 FileUtils.copyFile(tmp, dest);
		} catch (WebDriverException e) {
		System.err.println("Error while taking screenshot");
		
		} catch (IOException e) {
		System.err.println("Error while creating/copying the image file");
		}
			i++;
		}

}
