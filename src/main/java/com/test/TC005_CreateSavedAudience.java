package com.test;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.net.MalformedURLException;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.FluentWait;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.google.common.base.Function;

public class TC005_CreateSavedAudience extends ProjectWrappers{
	
	public  TC005_CreateSavedAudience() throws MalformedURLException {
		// TODO Auto-generated constructor stub
	}
	
	@BeforeClass
	public void beforeClass() {
	
		testName = "TC005_CreateSavedAudienceSuccess";
		description="To create a saved audience and verify";
		author="Aruna";
		category="Smoke";
			}
	
	public WebDriver driver=getDriver();
	int i=0;
	
	@Test(groups= {"SmokeTest"})
	public void savedAudience() throws InterruptedException ,IOException {

		Properties prop=new Properties();
      	BufferedReader reader=new BufferedReader(new FileReader("./Data/login.properties"));
    	prop.load(reader);  
    String env=prop.getProperty("env");
    String user=prop.getProperty("username");
    String pwd=prop.getProperty("password");
    
	System.out.println("Starting test: "+testName);
    invokeApp(env, true);
    Thread.sleep(3000);
    try {
    	driver.findElement(By.xpath("(//span[text()='Continue with Google'])[2]")).click();
    	Thread.sleep(3000);
    	enterByXpath("//input[@name='identifier']",user,"Username");
    	Thread.sleep(2000);
    	driver.findElement(By.xpath("//div[@id='identifierNext']")).click();
    	Thread.sleep(3000);
    	driver.findElement(By.xpath("//input[@name='password']")).sendKeys(pwd);
    	driver.findElement(By.xpath("//div[@id='passwordNext']")).click();
    	reportStep("Pass","Cadenz is launched successfully");
    	}
    	catch(Exception e) {
    		reportStep("Fail","Not able to launch Cadenz url");
    		takeSnap(i++);
    	}
    	
    	Thread.sleep(7000);
    	takeSnap(i++);
        	try {
    	WebElement filter=driver.findElement(By.xpath("(//div[@class='ant-select-selection__rendered'])[1]")); 
    	JavascriptExecutor exec=(JavascriptExecutor)driver;
    	exec.executeScript("arguments[0].click();", filter);
      	Thread.sleep(5000);
    	((JavascriptExecutor) driver).executeScript("window.scrollBy(0, -250)", "");
    	Thread.sleep(5000);
    	driver.findElement(By.xpath("//span[contains(text(),'Gender Type Description')]")).click();
    //	driver.findElement(By.xpath("//span[contains(text(),'Account Credit Limit')]")).click();
    	Thread.sleep(2000);
    	reportStep("Pass","Filter set successfully");
    	takeSnap(i++);
        	}
        	catch(Exception e) {
        		reportStep("Fail","Not able to set filter");
        		takeSnap(i++);
        	}
    	
    	try {
    	WebElement filter1=driver.findElement(By.xpath("(//div[@class='ant-select-selection__rendered'])[2]")); 
    	JavascriptExecutor exec1=(JavascriptExecutor)driver;
    	exec1.executeScript("arguments[0].click();", filter1);
    	driver.findElement(By.xpath("//li[text()='Not equals']")).click();
    	reportStep("Pass","Condition set successfully");
    	takeSnap(i++);
    	}
    	catch(Exception e) {
    		reportStep("Fail","Not able to set Condition");
    		takeSnap(i++);
    	}
	
    	
    	try {
    	WebElement selectField=driver.findElement(By.xpath("(//div[@class='ant-select-selection__rendered'])[3]")); 
    	JavascriptExecutor exec3=(JavascriptExecutor)driver;
    	exec3.executeScript("arguments[0].click();", selectField);
    	driver.findElement(By.xpath("//li[text()='Female']")).click();
    	reportStep("Pass","Attribute value set successfully");
    	takeSnap(i++);
    	}
    	catch(Exception e) {
    		reportStep("Fail","Not able to set Attribute value");
    		takeSnap(i++);
    	}
	
    	
    	try {
        	driver.findElement(By.xpath("//*[@id=\"conceptQueryText\"]/span/i")).click();
        	reportStep("Pass", "Clicked add filter icon");
        	}
        	catch(Exception e) {
        		reportStep("Fail", "Unable to click add filter icon to add into Additional filters section");
        	}
        	
    	//driver.findElement(By.xpath("//*[@id=\"conceptQueryText\"]/span/i")).click();
    	//click search button
    	Thread.sleep(3000);
    	try {
    	driver.findElement(By.xpath("//button[@id='mainSearchButton']")).click();
    	reportStep("Pass","Clicked on Search button successfully");
    	takeSnap(i++);
    	}
    	catch(Exception e) {
    		reportStep("Fail","Not able to click Search button");
    		takeSnap(i++);
    	}
    	
    	try {
        	FluentWait<WebDriver> wait = new FluentWait<WebDriver>(driver);
    		wait.pollingEvery(5,  TimeUnit.SECONDS);
    		wait.withTimeout(1, TimeUnit.MINUTES);
    		wait.ignoring(NoSuchElementException.class); //make sure that this exception is ignored
    		Function<WebDriver, WebElement> function = new Function<WebDriver, WebElement>()
    				{
    					public WebElement apply(WebDriver arg0) {
    						System.out.println("Checking for the element!!");
    						WebElement element = arg0.findElement(By.xpath("//div[@class='ant-tabs-tab-active ant-tabs-tab']"));
    						if(element != null)
    						{
    							System.out.println("Target element found");
    							element.click();
    							 
    						}
    						return element;
    					}
    				};

    		wait.until(function);
    		 reportStep("Pass", "The Search results are ready to the user");
        	}
    	
        	catch(Exception e) {
        		reportStep("Fail","Not able to get Search results");
        		takeSnap(i++);
        	}
clickByXpath("(//button[@class='ant-btn adv-search-btn ant-btn-primary'])[3]", "Save");
driver.findElement(By.xpath("//input[@id='audienceName']")).clear();
enterByXpath("//input[@id='audienceName']", "st2123", "Audience Name");
//driver.findElement(By.xpath("//span[text()='Create New']")).click();

WebElement createNew=driver.findElement(By.xpath("//span[text()='Create New']")); 
JavascriptExecutor exec4=(JavascriptExecutor)driver;
exec4.executeScript("arguments[0].click();", createNew);

Thread.sleep(2000);
try {
if(driver.findElement(By.xpath("//*[text()='Saved the audience successfully']")).isDisplayed())
	reportStep("pass", "The audience is saved and appropriate success message is displayed");
else
	reportStep("fail", "Audience is not saved ");
}
catch(Exception e) {
	reportStep("Fail","catch - Audience is not saved");
	takeSnap(i++);
}


    	takeSnap(i++);
    
   }
	
	public void takeSnap(int i) {
		 try {
			File tmp= ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
			 File dest= new File("./screenshots/Audience"+i+".png");
			 FileUtils.copyFile(tmp, dest);
		} catch (WebDriverException e) {
		System.err.println("Error while taking screenshot");
		
		} catch (IOException e) {
		System.err.println("Error while creating/copying the image file");
		}
			i++;
		}

}
