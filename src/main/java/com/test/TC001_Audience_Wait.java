package com.test;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.net.MalformedURLException;
import java.util.Collections;
import java.util.List;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.FileUtils;
import org.apache.poi.xdgf.usermodel.section.GeometrySection;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.Wait;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.google.common.base.Function;

public class TC001_Audience_Wait extends GenericWrappers{
	
	public  TC001_Audience_Wait() throws MalformedURLException {
		// TODO Auto-generated constructor stub
	}

	
	@BeforeClass
	public void beforeClass() {
	
		testName = "TC001_Audience";
		description="To verify Audience Filter search";
		author="Aruna";
		category="Smoke";
			}
	
		public WebDriver driver=getDriver();
	int i=0;
	
	@Test (alwaysRun = true)
	public void AudienceTestFilter() throws InterruptedException, IOException {
		//String url="https://35.154.166.184:8040/cadenz/audiences/audienceDiscovery";
		
//	new GenericWrappers(driver);
	/*	System.setProperty("webdriver.chrome.driver","./drivers/chromedriver.exe");
    		ChromeOptions options = new ChromeOptions();
    	options.addArguments("--no-sandbox","--incognito");
    	//	options.addArguments("--no-sandbox","--headless","--window-size=1200x600");
    	//chromeOptions.addArguments("--window-size=1200x600");
      	driver=new ChromeDriver(options);*/
      	
      	Properties prop=new Properties();
      	BufferedReader reader=new BufferedReader(new FileReader("./Data/login.properties"));
    	prop.load(reader);  
      	String url1=prop.getProperty("url");
      	
      //	startReport();
      	//startTest(testName, description);
      	
    	driver.get(url1);
    	driver.manage().window().maximize();
    	Thread.sleep(3000);
    	    		System.out.println("Driver is: "+driver);
    	
    	clickByXpath("//button[@id='details-button']","Advanced");
    		//driver.findElement(By.xpath("//button[@id='details-button']")).click();
    	Thread.sleep(3000);
    	clickByXpath("//a[@id='proceed-link']","Proceed");
    	//driver.findElement(By.xpath("//a[@id='proceed-link']")).click();
    	Thread.sleep(3000);
    	clickByXpath("(//span[text()='Continue with Google'])[2]","Continue with Google");
    	Thread.sleep(3000);
    	enterByXpath("//input[@name='identifier']",prop.getProperty("username"),"Username");
    	Thread.sleep(2000);
    	clickByXpath("//div[@id='identifierNext']","Next");
    	//driver.findElement(By.xpath("(//span[@class='RveJvd snByac'])[1]")).click();
    	Thread.sleep(3000);
    	driver.findElement(By.xpath("//input[@name='password']")).sendKeys(prop.getProperty("password"));
    	driver.findElement(By.xpath("//div[@id='passwordNext']")).click();
    	
    	reportStep("Pass","Cadenz is launched successfully");
    	
    	Thread.sleep(7000);
    	takeSnap(i++);
        	try {
    	WebElement filter=driver.findElement(By.xpath("(//div[@class='ant-select-selection__rendered'])[1]")); 
    	JavascriptExecutor exec=(JavascriptExecutor)driver;
    	exec.executeScript("arguments[0].click();", filter);
      	Thread.sleep(5000);
    	((JavascriptExecutor) driver).executeScript("window.scrollBy(0, -250)", "");
    	Thread.sleep(5000);
    	driver.findElement(By.xpath("(//span[contains(text(),'Brand Type Code')])[2]")).click();
    	Thread.sleep(2000);
    	reportStep("Pass","Filter set successfully");
    	takeSnap(i++);
        	}
        	catch(Exception e) {
        		reportStep("Fail","Not able to set filter");
        		takeSnap(i++);
        	}
    	
    	try {
    	WebElement filter1=driver.findElement(By.xpath("(//div[@class='ant-select-selection__rendered'])[2]")); 
    	JavascriptExecutor exec1=(JavascriptExecutor)driver;
    	exec1.executeScript("arguments[0].click();", filter1);
    	driver.findElement(By.xpath("//li[text()='Equals']")).click();
    	reportStep("Pass","Condition set successfully");
    	takeSnap(i++);
    	}
    	catch(Exception e) {
    		reportStep("Fail","Not able to set Condition");
    		takeSnap(i++);
    	}
	
    	
    	try {
    	WebElement selectField=driver.findElement(By.xpath("(//div[@class='ant-select-selection__rendered'])[3]")); 
    	JavascriptExecutor exec3=(JavascriptExecutor)driver;
    	exec3.executeScript("arguments[0].click();", selectField);
    	driver.findElement(By.xpath("//li[text()='GHP-PREPAID']")).click();
    	reportStep("Pass","Attribute value set successfully");
    	takeSnap(i++);
    	}
    	catch(Exception e) {
    		reportStep("Fail","Not able to set Attribute value");
    		takeSnap(i++);
    	}
    	try {
    	driver.findElement(By.xpath("//*[@id=\"conceptQueryText\"]/span/i")).click();
    	reportStep("Pass", "Clicked add filter icon");
    	}
    	catch(Exception e) {
    		reportStep("Fail", "Unable to click add filter icon to add into Additional filters section");
    	}
    	//click search button
    	Thread.sleep(3000);
    	try {
    	driver.findElement(By.xpath("//button[@id='mainSearchButton']")).click();
    	reportStep("Pass","Clicked on Search button successfully");
    	takeSnap(i++);
    	}
    	catch(Exception e) {
    		reportStep("Fail","Not able to click Search button");
    		takeSnap(i++);
    	}
    	//Thread.sleep(9000);
    	
    	
    /*	try {
    	//WebElement foo = driver.findElement(By.xpath("//div[@class='ant-tabs-tab-active ant-tabs-tab']"));

        new WebDriverWait(driver, 10)
        .pollingEvery(2, TimeUnit.SECONDS)
        .withTimeout(2, TimeUnit.MINUTES)
        .until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath("//div[@class='ant-tabs-tab-active ant-tabs-tab']"))));
        reportStep("Pass", "The Search results are ready to the user");
    	}
    	catch(Exception e) {
    		reportStep("Fail","Not able to get Search results");
    		takeSnap(i++);
    	}*/
    	
    	
    	try {
    	FluentWait<WebDriver> wait = new FluentWait<WebDriver>(driver);
		wait.pollingEvery(5,  TimeUnit.SECONDS);
		wait.withTimeout(4, TimeUnit.MINUTES);
		wait.ignoring(NoSuchElementException.class); //make sure that this exception is ignored
		Function<WebDriver, WebElement> function = new Function<WebDriver, WebElement>()
				{
					public WebElement apply(WebDriver arg0) {
						System.out.println("Checking for the element!!");
						WebElement element = arg0.findElement(By.xpath("//div[@class='ant-tabs-tab-active ant-tabs-tab']"));
						if(element != null)
						{
							System.out.println("Target element found");
							element.click();
							 
						}
						return element;
					}
				};

		wait.until(function);
		 reportStep("Pass", "The Search results are ready to the user");
    	}
    	catch(Exception e) {
    		reportStep("Fail","Not able to get Search results");
    		takeSnap(i++);
    	}
	
    	

    	
    	
    /*	try {
    	if(driver.findElement(By.xpath("//div[@class='ant-tabs-tab-active ant-tabs-tab']")).isDisplayed()) {
    		reportStep("Pass", "The Search results are ready to the user");
    	System.out.println("Search results are ready");
    	}
    	else
    		System.out.println("Results are not ready");
    	
    	Thread.sleep(9000);
    	}
    	catch(Exception e) {
    		reportStep("Fail", "Filter results are not ready, Please check");
    		takeSnap(i++);
    	}*/
    	takeSnap(i++);
    	endTest();
    	endReport();
    
   }
	
	public void takeSnap(int i) {
		 try {
			File tmp= ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
			 File dest= new File("./screenshots/Audience"+i+".png");
			 FileUtils.copyFile(tmp, dest);
		} catch (WebDriverException e) {
		System.err.println("Error while taking screenshot");
		
		} catch (IOException e) {
		System.err.println("Error while creating/copying the image file");
		}
			i++;
		}
	
}
