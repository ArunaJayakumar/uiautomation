package com.test;

import java.io.File;
import java.io.IOException;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.FileUtils;
import org.apache.poi.xdgf.usermodel.section.GeometrySection;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.google.common.base.Function;

public class TC001_Audience extends ProjectWrappers{
	
	@BeforeClass
	public void beforeClass() {
	
		testName = "TC001_Audience";
		description="To verify Audience Filter search";
		author="Aruna";
		category="Smoke";
			}
	
	public WebDriver driver;
	//public int i;
	@Test (alwaysRun = true)
	public void AudienceTestFilter() throws InterruptedException {
		String url="https://35.154.166.184:8040/cadenz/audiences/audienceDiscovery";
		System.setProperty("webdriver.chrome.driver","./drivers/chromedriver.exe");
    	int i=0;
    	ChromeOptions options = new ChromeOptions();
   // options.addArguments("--no-sandbox","--headless","--window-size=1200x600");
    	options.addArguments("--no-sandbox","--incognito");
    	
    	driver=new ChromeDriver(options);
    	  
    	driver.get(url);
    	driver.manage().window().maximize();
    	Thread.sleep(3000);
    //	try {
    	driver.findElement(By.xpath("//button[@id='details-button']")).click();
    	Thread.sleep(3000);
    	driver.findElement(By.xpath("//a[@id='proceed-link']")).click();
    	Thread.sleep(3000);
    	driver.findElement(By.xpath("(//span[text()='Continue with Google'])[2]")).click();
    	Thread.sleep(3000);
    	driver.findElement(By.xpath("//input[@name='identifier']")).sendKeys("aruna.jayakumar@thedatateam.in");
    	//Thread.sleep(2000);
    	//driver.findElement(By.xpath("(//span[@class='RveJvd snByac'])[1]")).click();
    	//driver.findElement(By.xpath("(//div[@class='VfPpkd-RLmnJb'])[1]")).click();
    	driver.findElement(By.xpath("//div[@id='identifierNext']")).click();
    	Thread.sleep(3000);
    	driver.findElement(By.xpath("//input[@name='password']")).sendKeys("1Thedatateampwd!");
    	driver.findElement(By.xpath("//div[@id='passwordNext']")).click();
    	//driver.findElement(By.xpath("(//span[@class='RveJvd snByac'])[1]")).click();
    	
    	reportStep("Pass","Cadenz is launched successfully");
    /*	}
    	catch(Exception e) {
    		reportStep("Fail","Not able to launch Cadenz url");
    		takeSnap(i++);
    	}
    	*/
    	Thread.sleep(7000);
    	takeSnap(i++);
        	try {
    	WebElement filter=driver.findElement(By.xpath("(//div[@class='ant-select-selection__rendered'])[1]")); 
    	JavascriptExecutor exec=(JavascriptExecutor)driver;
    	exec.executeScript("arguments[0].click();", filter);
      	Thread.sleep(5000);
    	((JavascriptExecutor) driver).executeScript("window.scrollBy(0, -250)", "");
    	Thread.sleep(5000);
    	driver.findElement(By.xpath("(//span[contains(text(),'Brand Type Code')])[2]")).click();
    	Thread.sleep(2000);
    	reportStep("Pass","Filter set successfully");
    	takeSnap(i++);
        	}
        	catch(Exception e) {
        		reportStep("Fail","Not able to set filter");
        		takeSnap(i++);
        	}
    	
    	try {
    	WebElement filter1=driver.findElement(By.xpath("(//div[@class='ant-select-selection__rendered'])[2]")); 
    	JavascriptExecutor exec1=(JavascriptExecutor)driver;
    	exec1.executeScript("arguments[0].click();", filter1);
    	driver.findElement(By.xpath("//li[text()='Equals']")).click();
    	reportStep("Pass","Condition set successfully");
    	takeSnap(i++);
    	}
    	catch(Exception e) {
    		reportStep("Fail","Not able to set Condition");
    		takeSnap(i++);
    	}
	
    	
    	try {
    	WebElement selectField=driver.findElement(By.xpath("(//div[@class='ant-select-selection__rendered'])[3]")); 
    	JavascriptExecutor exec3=(JavascriptExecutor)driver;
    	exec3.executeScript("arguments[0].click();", selectField);
    	driver.findElement(By.xpath("//li[text()='GHP-PREPAID']")).click();
    	reportStep("Pass","Attribute value set successfully");
    	takeSnap(i++);
    	}
    	catch(Exception e) {
    		reportStep("Fail","Not able to set Attribute value");
    		takeSnap(i++);
    	}
	
    	
    	try {
        	driver.findElement(By.xpath("//*[@id=\"conceptQueryText\"]/span/i")).click();
        	reportStep("Pass", "Clicked add filter icon");
        	}
        	catch(Exception e) {
        		reportStep("Fail", "Unable to click add filter icon to add into Additional filters section");
        	}
        	
    	//driver.findElement(By.xpath("//*[@id=\"conceptQueryText\"]/span/i")).click();
    	//click search button
    	Thread.sleep(3000);
    	try {
    	driver.findElement(By.xpath("//button[@id='mainSearchButton']")).click();
    	reportStep("Pass","Clicked on Search button successfully");
    	takeSnap(i++);
    	}
    	catch(Exception e) {
    		reportStep("Fail","Not able to click Search button");
    		takeSnap(i++);
    	}
    	
    	try {
        	FluentWait<WebDriver> wait = new FluentWait<WebDriver>(driver);
    		wait.pollingEvery(5,  TimeUnit.SECONDS);
    		wait.withTimeout(4, TimeUnit.MINUTES);
    		wait.ignoring(NoSuchElementException.class); //make sure that this exception is ignored
    		Function<WebDriver, WebElement> function = new Function<WebDriver, WebElement>()
    				{
    					public WebElement apply(WebDriver arg0) {
    						System.out.println("Checking for the element!!");
    						WebElement element = arg0.findElement(By.xpath("//div[@class='ant-tabs-tab-active ant-tabs-tab']"));
    						if(element != null)
    						{
    							System.out.println("Target element found");
    							element.click();
    							 
    						}
    						return element;
    					}
    				};

    		wait.until(function);
    		 reportStep("Pass", "The Search results are ready to the user");
        	}
        	catch(Exception e) {
        		reportStep("Fail","Not able to get Search results");
        		takeSnap(i++);
        	}

    	takeSnap(i++);
    
   }
	
	public void takeSnap(int i) {
		 try {
			File tmp= ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
			 File dest= new File("./screenshots/Audience"+i+".png");
			 FileUtils.copyFile(tmp, dest);
		} catch (WebDriverException e) {
		System.err.println("Error while taking screenshot");
		
		} catch (IOException e) {
		System.err.println("Error while creating/copying the image file");
		}
			i++;
		}

}
