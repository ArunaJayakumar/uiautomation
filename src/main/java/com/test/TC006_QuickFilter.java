package com.test;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.net.MalformedURLException;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.google.common.base.Function;

public class TC006_QuickFilter extends ProjectWrappers{
	
	public  TC006_QuickFilter() throws MalformedURLException {
		// TODO Auto-generated constructor stub
	}

	@BeforeClass
	public void beforeClass() {
	
		testName = "TC006_QuickFilter";
		description="To verify Quick Filter search";
		author="Aruna";
		category="Smoke";
			}
	
	public WebDriver driver=getDriver();
	int i=0;
	
	@Test (groups= {"SmokeTest"})
	public void AudienceTestFilter() throws InterruptedException ,IOException {
	
		
		Properties prop=new Properties();
      	BufferedReader reader=new BufferedReader(new FileReader("./Data/login.properties"));
    	prop.load(reader);  
    String env=prop.getProperty("env");
    String user=prop.getProperty("username");
    String pwd=prop.getProperty("password");
    
	System.out.println("Starting test: "+testName);
	
    invokeApp(env, true);
    	Thread.sleep(3000);
    	
    	try {
    	driver.findElement(By.xpath("(//span[text()='Continue with Google'])[2]")).click();
    
    	Thread.sleep(3000);
    	enterByXpath("//input[@name='identifier']",user,"Username");
    	Thread.sleep(2000);
    	driver.findElement(By.xpath("//div[@id='identifierNext']")).click();
    	Thread.sleep(3000);
    	driver.findElement(By.xpath("//input[@name='password']")).sendKeys(pwd);
    	driver.findElement(By.xpath("//div[@id='passwordNext']")).click();
    	
    	
    	reportStep("Pass","Cadenz is launched successfully");
    	}
    	catch(Exception e) {
    		reportStep("Fail","Not able to launch Cadenz url");
    		takeSnap(i++);
    	}
    	
    	Thread.sleep(7000);
    	takeSnap(i++);
    	Actions act=new Actions(driver);
        	try {
    	
    
    	WebDriverWait wait=new WebDriverWait(driver, 5);
    	wait.until(ExpectedConditions.elementToBeClickable(driver.findElement(By.xpath("//div[@class='MuiPaper-root MuiCard-root MuiPaper-elevation3 MuiPaper-rounded']"))));
    	driver.findElement(By.xpath("//div[@class='MuiPaper-root MuiCard-root MuiPaper-elevation3 MuiPaper-rounded']")).click();
    	driver.findElement(By.xpath("//input[@value='Female']")).click();
    	
    	WebElement quicksearch=driver.findElement(By.xpath("//input[@class='jss53']"));
    	Actions quick=new Actions(driver);
    	quick.moveToElement(quicksearch).build().perform();    	
    	clickByXpath("//input[@class='jss53']", "QuickFilter Submit");
    	reportStep("Pass","Clicked on Search button successfully");
    	takeSnap(i++);
    	}
    	catch(Exception e) {
    		reportStep("Fail","Not able to set Quick Filter");
    		takeSnap(i++);
    	}
        	
    	try {
    		WebElement mainSearch=driver.findElement(By.xpath("(//span[@class='MuiButton-label'])[1]"));
    		act.moveToElement(mainSearch).build().perform();
        	driver.findElement(By.xpath("(//span[@class='MuiButton-label'])[1]")).click();
        	reportStep("Pass","Clicked on Search button successfully");
        	takeSnap(i++);
        	}
        	catch(Exception e) {
        		reportStep("Fail","Not able to click Search button");
        		takeSnap(i++);
        	}
        	
    	Thread.sleep(5000);
      	
    	try {
        	FluentWait<WebDriver> wait1 = new FluentWait<WebDriver>(driver);
    		wait1.pollingEvery(5,  TimeUnit.SECONDS);
    		wait1.withTimeout(2, TimeUnit.MINUTES);
    		wait1.ignoring(NoSuchElementException.class); //make sure that this exception is ignored
    		Function<WebDriver, WebElement> function = new Function<WebDriver, WebElement>()
    				{
    					public WebElement apply(WebDriver arg0) {
    						System.out.println("Checking for the element!!");
    						WebElement element = arg0.findElement(By.xpath("//span[text()='Audience Insights']"));
    						if(element != null)
    						{
    							System.out.println("Target element found");
    							element.click();
    							 
    						}
    						return element;
    					}
    				};

    		wait1.until(function);
    		 reportStep("Pass", "The Search results are ready to the user");
        	}
    	
        	catch(Exception e) {
        		reportStep("Fail","Not able to get Search results");
        		takeSnap(i++);
        	}

    	takeSnap(i++);
    
   }
	
	public void takeSnap(int i) {
		 try {
			File tmp= ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
			 File dest= new File("./screenshots/Audience"+i+".png");
			 FileUtils.copyFile(tmp, dest);
		} catch (WebDriverException e) {
		System.err.println("Error while taking screenshot");
		
		} catch (IOException e) {
		System.err.println("Error while creating/copying the image file");
		}
			i++;
		}

}
