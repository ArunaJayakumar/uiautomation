package com.test;

import java.time.Duration;
import java.time.Instant;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Date;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

/**
 * Unit test for simple App.
 */
public class AppTest {
	
	@SuppressWarnings("deprecation")
	public static void main(String[] args) {
		DateTimeFormatter dtf = DateTimeFormatter.ofPattern("dd MMM yyyy");  
 	   LocalDate now = LocalDate.now();  
 	   System.out.println(dtf.format(now));
 	   
 	   Date today=Date.from(Instant.now());
 	   int todayDate=today.getDate();
 	  Date xDaysAgo = Date.from( Instant.now().minus( Duration.ofDays( 2 ) ) );
 	  
 	  System.out.println("2 days ago: "+xDaysAgo);
 	  System.out.println(xDaysAgo.getDate());
 	
	}
  }
