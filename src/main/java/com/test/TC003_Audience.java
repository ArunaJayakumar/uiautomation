package com.test;

import java.io.BufferedReader;
import java.net.MalformedURLException;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.Properties;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.google.common.base.Function;

public class TC003_Audience extends ProjectWrappers{
	
	public  TC003_Audience() throws MalformedURLException {
		// TODO Auto-generated constructor stub
	}
	
	@BeforeClass
	public void beforeClass() {
	
		testName = "TC003_Audience";
		description="To verify Audience Filter search";
		author="Aruna";
		category="Smoke";
			}
	
	public WebDriver driver=getDriver();
	int i=0;
	//@Parameters({ "environment" })
	@Test (groups= {"SmokeTest"})
	public void AudienceTestFilter() throws InterruptedException ,IOException {
		Properties prop=new Properties();
      	BufferedReader reader=new BufferedReader(new FileReader("./Data/login.properties"));
    	prop.load(reader);  
    String env=prop.getProperty("env");
    String user=prop.getProperty("username");
    String pwd=prop.getProperty("password");
	System.out.println("Starting test: "+testName);
		invokeApp(env, true);

	//	String url="https://35.154.166.184:8040/cadenz/audiences/audienceDiscovery";
	/*	System.setProperty("webdriver.chrome.driver","./drivers/chromedriver.exe");
    	int i=0;
    	ChromeOptions options = new ChromeOptions();
   // options.addArguments("--no-sandbox","--headless","--window-size=1200x600");
    	options.addArguments("--no-sandbox","--incognito");
    	
    	driver=new ChromeDriver(options);*/
    	driver.findElement(By.xpath("(//span[text()='Continue with Google'])[2]")).click();
    	//clickByXpath("(//span[text()='Continue with Google'])[2]","Continue with Google");
    	Thread.sleep(3000);
    	enterByXpath("//input[@name='identifier']",user,"Username");
    	Thread.sleep(2000);
    	//driver.findElement(By.xpath("(//span[@class='RveJvd snByac'])[1]")).click();
    	//driver.findElement(By.xpath("(//div[@class='VfPpkd-RLmnJb'])[1]")).click();
    	driver.findElement(By.xpath("//div[@id='identifierNext']")).click();
    	Thread.sleep(3000);
    	driver.findElement(By.xpath("//input[@name='password']")).sendKeys(pwd);
    	driver.findElement(By.xpath("//div[@id='passwordNext']")).click();
    	//driver.findElement(By.xpath("(//span[@class='RveJvd snByac'])[1]")).click();
    	
    	//reportStep("Pass","Cadenz is launched successfully");
    /*	}
    	catch(Exception e) {
    		reportStep("Fail","Not able to launch Cadenz url");
    		takeSnap(i++);
    	}
    	*/
    	Thread.sleep(7000);
    	takeSnap(i++);
        	//try {
        		Actions action=new Actions(driver);
            	WebElement move=driver.findElement(By.xpath("//div[@class='profiles-date-main']"));
            	action.moveToElement(move).build().perform();
            	clickByXpath("//input[@id='size-small-outlined']","Attribute Filter");
            	enterByXpath("//input[@id='size-small-outlined']", "Previous Subscriber Key","Filter set");
            	
            WebElement filter=	driver.findElement(By.xpath("//input[@aria-controls='size-small-outlined-popup']"));
            JavascriptExecutor exec=(JavascriptExecutor)driver;
        	exec.executeScript("arguments[0].click();", filter);
          	
            //	clickByXpath("//input[@aria-activedescendant='size-small-outlined-option-0']", "Option 0");
            	//clickByXpath("//input[@value='Previous Subscriber Key']","Previous Subscriber Key");
    /*	
    	Thread.sleep(5000);
    	((JavascriptExecutor) driver).executeScript("window.scrollBy(0, -250)", "");
    	Thread.sleep(5000);
    	driver.findElement(By.xpath("(//span[contains(text(),'Brand Type Code')])[2]")).click();
    	Thread.sleep(2000);
    	reportStep("Pass","Filter set successfully");
    	takeSnap(i++);
        	}
        	catch(Exception e) {
        		reportStep("Fail","Not able to set filter");
        		takeSnap(i++);
        	}*/
    	
    	try {
    	WebElement filter1=driver.findElement(By.xpath("(//div[@class='ant-select-selection__rendered'])[2]")); 
    	JavascriptExecutor exec1=(JavascriptExecutor)driver;
    	exec1.executeScript("arguments[0].click();", filter1);
    	driver.findElement(By.xpath("//li[text()='Equals']")).click();
    	reportStep("Pass","Condition set successfully");
    	takeSnap(i++);
    	}
    	catch(Exception e) {
    		reportStep("Fail","Not able to set Condition");
    		takeSnap(i++);
    	}
	
    	
    	try {
    	WebElement selectField=driver.findElement(By.xpath("(//div[@class='ant-select-selection__rendered'])[3]")); 
    	JavascriptExecutor exec3=(JavascriptExecutor)driver;
    	exec3.executeScript("arguments[0].click();", selectField);
    	driver.findElement(By.xpath("//li[text()='GHP-PREPAID']")).click();
    	reportStep("Pass","Attribute value set successfully");
    	takeSnap(i++);
    	}
    	catch(Exception e) {
    		reportStep("Fail","Not able to set Attribute value");
    		takeSnap(i++);
    	}
	
    	
    	try {
        	driver.findElement(By.xpath("//*[@id=\"conceptQueryText\"]/span/i")).click();
        	reportStep("Pass", "Clicked add filter icon");
        	}
        	catch(Exception e) {
        		reportStep("Fail", "Unable to click add filter icon to add into Additional filters section");
        	}
        	
    	//driver.findElement(By.xpath("//*[@id=\"conceptQueryText\"]/span/i")).click();
    	//click search button
    	Thread.sleep(3000);
    	try {
    	driver.findElement(By.xpath("//button[@id='mainSearchButton']")).click();
    	reportStep("Pass","Clicked on Search button successfully");
    	takeSnap(i++);
    	}
    	catch(Exception e) {
    		reportStep("Fail","Not able to click Search button");
    		takeSnap(i++);
    	}
   /* 	
    	try {
        	FluentWait<WebDriver> wait = new FluentWait<WebDriver>(driver);
    		wait.pollingEvery(5,  TimeUnit.SECONDS);
    		wait.withTimeout(1, TimeUnit.MINUTES);
    		wait.ignoring(NoSuchElementException.class); //make sure that this exception is ignored
    		Function<WebDriver, WebElement> function = new Function<WebDriver, WebElement>()
    				{
    					public WebElement apply(WebDriver arg0) {
    						System.out.println("Checking for the element!!");
    						WebElement element = arg0.findElement(By.xpath("//div[@class='ant-tabs-tab-active ant-tabs-tab']"));
    						if(element != null)
    						{
    							System.out.println("Target element found");
    							element.click();
    							 
    						}
    						return element;
    					}
    				};

    		wait.until(function);
    		 reportStep("Pass", "The Search results are ready to the user");
        	}
    	
        	catch(Exception e) {
        		reportStep("Fail","Not able to get Search results");
        		takeSnap(i++);
        	}

    	takeSnap(i++);*/
    
   }
	
	public void takeSnap(int i) {
		 try {
			File tmp= ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
			 File dest= new File("./screenshots/Audience"+i+".png");
			 FileUtils.copyFile(tmp, dest);
		} catch (WebDriverException e) {
		System.err.println("Error while taking screenshot");
		
		} catch (IOException e) {
		System.err.println("Error while creating/copying the image file");
		}
			i++;
		}

}
