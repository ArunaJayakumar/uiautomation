package com.test;

import java.io.BufferedReader;
import io.github.bonigarcia.wdm.WebDriverManager;
import java.io.FileReader;
import java.io.IOException;
import java.net.MalformedURLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Duration;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.Date;
import java.util.Properties;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class TC001_DataRecency extends ProjectWrappers {
	
	public  TC001_DataRecency() throws MalformedURLException {
		// TODO Auto-generated constructor stub
	}

	@BeforeClass (alwaysRun=true)
	public void beforeClass(){
	
		testName = "TC001_DateCheck";
		description="To verify Latest Profile Date";
		author="Aruna";
		category="Smoke";
	}
	
		public WebDriver driver=getDriver();
		int i=0;
		
		@Test(groups= {"SmokeTest"})
		public void dateCheck() throws InterruptedException, IOException, ParseException {
		
		 	Properties prop=new Properties();
	      	BufferedReader reader=new BufferedReader(new FileReader("./Data/login.properties"));
	    	prop.load(reader);  
	    String env=prop.getProperty("env");
	    String user=prop.getProperty("username");
	    String pwd=prop.getProperty("password");  
        
		System.out.println("Starting test: "+testName);
		
    	invokeApp(env,true);
    	Thread.sleep(3000);
    	reportStep("Pass", "Launching Cadenz in '"+env+"' environment");
    	try {
    	driver.findElement(By.xpath("(//span[text()='Continue with Google'])[2]")).click();
    	Thread.sleep(3000);
       	driver.findElement(By.xpath("//input[@name='identifier']")).sendKeys(user);
    	driver.findElement(By.xpath("//div[@id='identifierNext']")).click();
    	Thread.sleep(3000);
    	driver.findElement(By.xpath("//input[@name='password']")).sendKeys(pwd);
    	driver.findElement(By.xpath("//div[@id='passwordNext']")).click();
		}
    	catch(Exception e) {
    		reportStep("Fail","Not able to launch Cadenz");
    	}
    	Thread.sleep(10000);
    	WebDriverWait wait = new WebDriverWait(driver, 20);
    //try {	
    	Actions action=new Actions(driver);
    	WebElement filter=driver.findElement(By.xpath("//div[@class='profiles-date-main']"));
    	Thread.sleep(5000);
    	action.moveToElement(filter).build().perform();
    	String text=driver.findElement(By.xpath("//div[@class='profiles-date-main']")).getText();
    	System.out.println("Text is: "+text);
    	String subtext=text.substring(22);
    	System.out.println("subtext part is: "+subtext);
    	//get current system date
    	DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MMM/dd");  
    	   LocalDate now = LocalDate.now();  
    	   System.out.println("Today is: "+dtf.format(now)); 
    	   
    	   String year=subtext.substring((subtext.length())-4);
    	   String month=subtext.substring((subtext.length()-8),(subtext.length())-5);
    	   String day=subtext.substring((subtext.length()-14),(subtext.length())-12);
    	   
    	   System.out.println("Year: "+year+" and Month is: "+month+" and Date is: "+day);
    	   String dateString=day+"/"+month+"/"+year;
    	   System.out.println("Date String= "+dateString);
    	   SimpleDateFormat cadFormat = new SimpleDateFormat("yyyy/MMM/dd");
    	   Date dateOnCadenz=new SimpleDateFormat("dd/MMM/yyyy").parse(dateString);
    	   String mdy = cadFormat.format(dateOnCadenz);
    	System.out.println("Date on Cadenz to date format: "+dateOnCadenz);
    	System.out.println("Date after format: "+mdy);
    	
    	LocalDate diff=now.plusDays(-2);
    	
    	System.out.println("Difference is: "+diff);
    	 
    	
    	reportStep("Pass", "Date&Time on Homepage is: '"+subtext+"'");
  /*  }
	catch(Exception e) {
		reportStep("Fail","Not able to locate Profile Date field");
	}*/
	
	
}
}
