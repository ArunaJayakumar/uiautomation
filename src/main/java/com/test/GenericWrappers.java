package com.test;

import java.lang.reflect.Method;
import java.net.MalformedURLException;
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.By;
import org.openqa.selenium.ElementNotInteractableException;
import org.openqa.selenium.ElementNotSelectableException;
import org.openqa.selenium.ElementNotVisibleException;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;


public class GenericWrappers extends Reports{
	public WebDriver driver;
	public String testName, description;
	public String browser;
	public String author;
	public String category;
	public Method method;
	
	//DesiredCapabilities dc = DesiredCapabilities.chrome();
	
	//dc.setBrowserName("chrome");
	
	public WebDriver getDriver() throws MalformedURLException {
		//WebDriverManager.chromedriver().setup();
		System.setProperty("webdriver.chrome.driver","./drivers/chromedriver");
		ChromeOptions options = new ChromeOptions();
	options.addArguments("--no-sandbox","--incognito","--headless","--disable-dev-shm-usage");
	//driver=new RemoteWebDriver(new URL("http://localhost:9515"), DesiredCapabilities.chrome());
	driver=new ChromeDriver();
		return this.driver;
	}
	/* 	
   	driver=new ChromeDriver();
   	String url="https://35.154.166.184:8040/cadenz/audiences/audienceDiscovery";
	System.setProperty("webdriver.chrome.driver","./drivers/chromedriver.exe");
	driver.get(url);
	driver.manage().window().maximize();
	Thread.sleep(3000);
	
	*/
	
	/*
	public GenericWrappers(RemoteWebDriver driver) {
		this.driver=driver;
	}
	int i=1;*/
	
	public void invokeApp(String env,boolean flag) throws InterruptedException {
		String url=null;
		if(env.equalsIgnoreCase("dev"))
			url="https://35.154.166.184:8040/cadenz/audiences/audienceDiscovery";
		else if(env.equalsIgnoreCase("globedev"))
			url="https://uup-dev.globe.com.ph/cadenz/audiences/audienceDiscovery";
		else if(env.equalsIgnoreCase("prod"))
			url="https://uup.globe.com.ph/cadenz/audiences/audienceDiscovery";
		System.out.println(url);
	/*	DesiredCapabilities dc = new DesiredCapabilities();
		dc.setPlatform(Platform.WINDOWS);
		dc.setBrowserName(browser);*/
		try {
		/*	System.setProperty("webdriver.chrome.driver","C:\\Users\\TDT\\Downloads\\Selenium_workspace\\CadenzAI\\drivers\\chromedriver.exe");
			ChromeOptions options = new ChromeOptions();
		options.addArguments("--no-sandbox","--incognito");
		driver=new ChromeDriver(options);*/
			
			driver.manage().window().maximize();
			driver.get(url);
			driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
			
			if(env.equalsIgnoreCase("dev")){
				clickByXpath("//button[@id='details-button']", "Details");
		    	//driver.findElement(By.xpath("//button[@id='details-button']")).click();
		    	Thread.sleep(3000);
		    	driver.findElement(By.xpath("//a[@id='proceed-link']")).click();
		    	Thread.sleep(3000);
		    	
			}
		reportStep("The browser launched successfully and loaded the url - "+url, "PASS");
	
		} catch (WebDriverException e) {
		e.printStackTrace();
			reportStep("FAIL", "Unable to launch the browser due to unknown error");	
		
		}
		
	}
	public void enterByXpath(String xpathValue, String data, String fieldName) {
	
		try {
			driver.findElement(By.xpath(xpathValue)).sendKeys(data);
			System.out.println("The value '"+data+"' is entered in the field '"+fieldName);
			reportStep("Pass","The value '"+data+"' is entered in the field '"+fieldName);
		} catch (NoSuchElementException e) {
			reportStep("Fail","The field '"+fieldName+"' is not found in DOM");
		} catch (ElementNotVisibleException e) {
			reportStep("Fail","The field '"+fieldName+"' is not visible in the WebPage");
		} catch (ElementNotInteractableException e) {
			reportStep("Fail","The field '"+fieldName+"' is not interactable in the WebPage");
		} catch (ElementNotSelectableException e) {
			reportStep("Fail","The field '"+fieldName+"' is not selectable in the WebPage");
		} catch (StaleElementReferenceException e) {
			reportStep("Fail","The field '"+fieldName+"' is no longer available in the WebPage");
		} catch (WebDriverException e) {
			reportStep("Fail","Not able to find/interact with The field '"+fieldName);
		}
		}


	public void clickByXpath(String xpathVal, String fieldName) {
	
		try {
			driver.findElement(By.xpath(xpathVal)).click();
			reportStep("Pass","The field '"+fieldName+"' is clicked");
		} catch (NoSuchElementException e) {
			reportStep("Fail","The field '"+fieldName+"' is not found in DOM");
		} catch (ElementNotVisibleException e) {
			reportStep("Fail","The field '"+fieldName+"' is not visible in the WebPage");
		} catch (ElementNotInteractableException e) {
			reportStep("Fail","The field '"+fieldName+"' is not interactable in the WebPage");
		} catch (ElementNotSelectableException e) {
			reportStep("Fail","The field '"+fieldName+"' is not selectable in the WebPage");
		} catch (StaleElementReferenceException e) {
			reportStep("Fail","The field '"+fieldName+"' is no longer available in the WebPage");
		} 	
		catch (WebDriverException e) {
			reportStep("Fail","Not able to find/interact with The field '"+fieldName);
		}
			
	}

	public void clickByXpathNoSnap(String xpathVal, String fieldName) {
	
		try {
			driver.findElement(By.xpath(xpathVal)).click();
			reportStep("Pass","The field with XPath '"+xpathVal+"' is clicked");
		} catch (NoSuchElementException e) {
			reportStep("Fail","The field '"+fieldName+"' is not found in DOM");
		} catch (ElementNotVisibleException e) {
			reportStep("Fail","The field '"+fieldName+"' is not visible in the WebPage");
		} catch (ElementNotInteractableException e) {
			reportStep("Fail","The field '"+fieldName+"' is not interactable in the WebPage");
		} catch (ElementNotSelectableException e) {
			reportStep("Fail","The field '"+fieldName+"' is not selectable in the WebPage");
		} catch (StaleElementReferenceException e) {
			reportStep("Fail","The field '"+fieldName+"' is no longer available in the WebPage");
		} 	
		catch (WebDriverException e) {
			reportStep("Fail","Not able to find/interact with The field '"+fieldName);
		}
	}
	
	
	/*public void verifyTextById(String id, String text) {
		try {
			String textValue=driver.findElementById(id).getText();
			if(textValue.equals(text)) {
				reportStep("Pass","The text value "+text+"' is as expected");
			}
				else {
					reportStep("Pass","The expected text value at the field '"+id+"' is '"+text+"' but the actual text is '"+textValue+"'");	
			}
		} catch (NoSuchElementException e) {
			reportStep("Fail","The element with Id '"+id+"' is not found in DOM");
		} catch (ElementNotVisibleException e) {
			reportStep("Fail","The element with Id '"+id+"' is not visible in the WebPage");
		} catch (ElementNotInteractableException e) {
			reportStep("Fail","The element with Id '"+id+"' is not interactable in the WebPage");
		} catch (ElementNotSelectableException e) {
			reportStep("Fail","The element with Id '"+id+"' is not selectable in the WebPage");
		} catch (StaleElementReferenceException e) {
			reportStep("Fail","The element with Id '"+id+"' is no longer available in the WebPage");
		} catch (WebDriverException e) {
			reportStep("Fail","Not able to find/interact with the element with Id '"+id);
		}
		}

	public void verifyTextByXpath(String xpath, String text) {
		try {
			String textValue=driver.findElement(xpath).getText();
			if(textValue.equals(text)) {
				reportStep("Pass","The text value "+text+"' is as expected");
			}
				else {
					reportStep("Pass","The expected text value at the field '"+xpath+"' is '"+text+"' but the actual text is '"+textValue+"'");	
			}
		} catch (NoSuchElementException e) {
			reportStep("Fail","The element with XPath '"+xpath+"' is not found in DOM");
		} catch (ElementNotVisibleException e) {
			reportStep("Fail","The element with XPath '"+xpath+"' is not visible in the WebPage");
		} catch (ElementNotInteractableException e) {
			reportStep("Fail","The element with XPath '"+xpath+"' is not interactable in the WebPage");
		} catch (ElementNotSelectableException e) {
			reportStep("Fail","The element with XPath '"+xpath+"' is not selectable in the WebPage");
		} catch (StaleElementReferenceException e) {
			reportStep("Fail","The element with XPath '"+xpath+"' is no longer available in the WebPage");
		} catch (WebDriverException e) {
			reportStep("Fail","Not able to find/interact with the element with XPath '"+xpath);
		}
}

	public void verifyTextContainsByXpath(String xpath, String text) {
	
		try {
			String textValue=driver.findElement(xpath).getText();
			if(textValue.contains(text)) {
				reportStep("Pass","The text value "+text+"' is as expected");
			}
				else {
					reportStep("Pass","The actual text '"+textValue+"' does not contain the expected text '"+text+"'");	
			}
		} catch (NoSuchElementException e) {
			reportStep("Fail","The element with XPath '"+xpath+"' is not found in DOM");
		} catch (ElementNotVisibleException e) {
			reportStep("Fail","The element with XPath '"+xpath+"' is not visible in the WebPage");
		} catch (ElementNotInteractableException e) {
			reportStep("Fail","The element with XPath '"+xpath+"' is not interactable in the WebPage");
		} catch (ElementNotSelectableException e) {
			reportStep("Fail","The element with XPath '"+xpath+"' is not selectable in the WebPage");
		} catch (StaleElementReferenceException e) {
			reportStep("Fail","The element with XPath '"+xpath+"' is no longer available in the WebPage");
		}  catch (WebDriverException e) {
			reportStep("Fail","Not able to find/interact with the element with XPath '"+xpath);
		}
	}

	public void clickById(String id) {
	
		try {
			driver.findElementById(id).click();
			reportStep("Pass","The field with id '"+id+"' is clicked");
		} catch (NoSuchElementException e) {
			reportStep("Fail","The element with Id '"+id+"' is not found in DOM");
		} catch (ElementNotVisibleException e) {
			reportStep("Fail","The element with Id '"+id+"' is not visible in the WebPage");
		} catch (ElementNotInteractableException e) {
			reportStep("Fail","The element with Id '"+id+"' is not interactable in the WebPage");
		} catch (ElementNotSelectableException e) {
			reportStep("Fail","The element with Id '"+id+"' is not selectable in the WebPage");
		} catch (StaleElementReferenceException e) {
			reportStep("Fail","The element with Id '"+id+"' is no longer available in the WebPage");
		} 	
		catch (WebDriverException e) {
			reportStep("Fail","Not able to find/interact with the element with Id '"+id);
		}
		
	}

	public void clickByClassName(String classVal) {
	
		try {
			driver.findElementByClassName(classVal).click();
			reportStep("Pass","The field with ClassName '"+classVal+"' is clicked");
		} catch (NoSuchElementException e) {
			reportStep("Fail","The element with ClassName '"+classVal+"' is not found in DOM");
		} catch (ElementNotVisibleException e) {
			reportStep("Fail","The element with ClassName '"+classVal+"' is not visible in the WebPage");
		} catch (ElementNotInteractableException e) {
			reportStep("Fail","The element with ClassName '"+classVal+"' is not interactable in the WebPage");
		} catch (ElementNotSelectableException e) {
			reportStep("Fail","The element with ClassName '"+classVal+"' is not selectable in the WebPage");
		} catch (StaleElementReferenceException e) {
			reportStep("Fail","The element with ClassName '"+classVal+"' is no longer available in the WebPage");
		} 	
		catch (WebDriverException e) {
			reportStep("Fail","Not able to find/interact with the element with ClassName '"+classVal);
		}
			
	}

	public void clickByName(String name) {
		try {
			driver.findElementByName(name).click();
			reportStep("Pass","The field with Name '"+name+"' is clicked");
		} catch (NoSuchElementException e) {
			reportStep("Fail","The element with Name '"+name+"' is not found in DOM");
		} catch (ElementNotVisibleException e) {
			reportStep("Fail","The element with Name '"+name+"' is not visible in the WebPage");
		} catch (ElementNotInteractableException e) {
			reportStep("Fail","The element with Name '"+name+"' is not interactable in the WebPage");
		} catch (ElementNotSelectableException e) {
			reportStep("Fail","The element with Name '"+name+"' is not selectable in the WebPage");
		} catch (StaleElementReferenceException e) {
			reportStep("Fail","The element with Name '"+name+"' is no longer available in the WebPage");
		} 	
		catch (WebDriverException e) {
			reportStep("Fail","Not able to find/interact with the element with Name '"+name);
		}
		
	}

	public void clickByLink(String name) {
		try {
			driver.findElementByLinkText(name).click();
			reportStep("Pass","The field with Link '"+name+"' is clicked");
		} catch (NoSuchElementException e) {
			reportStep("Fail","The element with Link '"+name+"' is not found in DOM");
		} catch (ElementNotVisibleException e) {
			reportStep("Fail","The element with Link '"+name+"' is not visible in the WebPage");
		} catch (ElementNotInteractableException e) {
			reportStep("Fail","The element with Link '"+name+"' is not interactable in the WebPage");
		} catch (ElementNotSelectableException e) {
			reportStep("Fail","The element with Link '"+name+"' is not selectable in the WebPage");
		} catch (StaleElementReferenceException e) {
			reportStep("Fail","The element with Link '"+name+"' is no longer available in the WebPage");
		} 	
		catch (WebDriverException e) {
			reportStep("Fail","Not able to find/interact with the element with Link '"+name);
		}
			
	}

	public void clickByLinkNoSnap(String name) {
		try {
			driver.findElementByLinkText(name).click();
			reportStep("Pass","The field with Link '"+name+"' is clicked");
		} catch (NoSuchElementException e) {
			reportStep("Fail","The element with Link '"+name+"' is not found in DOM");
		} catch (ElementNotVisibleException e) {
			reportStep("Fail","The element with Link '"+name+"' is not visible in the WebPage");
		} catch (ElementNotInteractableException e) {
			reportStep("Fail","The element with Link '"+name+"' is not interactable in the WebPage");
		} catch (ElementNotSelectableException e) {
			reportStep("Fail","The element with Link '"+name+"' is not selectable in the WebPage");
		} catch (StaleElementReferenceException e) {
			reportStep("Fail","The element with Link '"+name+"' is no longer available in the WebPage");
		} 	
		catch (WebDriverException e) {
			reportStep("Fail","Not able to find/interact with the element with Link '"+name);
		}
		
		
	}


	public String getTextById(String idVal) {
		String textValue=null;
		try {
			 textValue=driver.findElementById(idVal).getText();
				reportStep("Pass","The text value at the id '"+idVal+"'is "+textValue+"' ");
		} catch (NoSuchElementException e) {
			reportStep("Fail","The element with Id '"+idVal+"' is not found in DOM");
		} catch (ElementNotVisibleException e) {
			reportStep("Fail","The element with Id '"+idVal+"' is not visible in the WebPage");
		} catch (ElementNotInteractableException e) {
			reportStep("Fail","The element with Id '"+idVal+"' is not interactable in the WebPage");
		} catch (ElementNotSelectableException e) {
			reportStep("Fail","The element with Id '"+idVal+"' is not selectable in the WebPage");
		} catch (StaleElementReferenceException e) {
			reportStep("Fail","The element with Id '"+idVal+"' is no longer available in the WebPage");
		} catch (WebDriverException e) {
			reportStep("Fail","Not able to find/interact with the element with Id '"+idVal);
		}
				
		return textValue;
		
	}

	public String getTextByXpath(String xpathVal) {
		String value=null;
		try {
			 value=driver.findElement(xpathVal).getText();
				reportStep("Pass","The text value at the element corresponding the the XPath '"+xpathVal+"'is "+value+"' ");
		} catch (NoSuchElementException e) {
			reportStep("Fail","The field '"+fieldName+"' is not found in DOM");
		} catch (ElementNotVisibleException e) {
			reportStep("Fail","The field '"+fieldName+"' is not visible in the WebPage");
		} catch (ElementNotInteractableException e) {
			reportStep("Fail","The field '"+fieldName+"' is not interactable in the WebPage");
		} catch (ElementNotSelectableException e) {
			reportStep("Fail","The field '"+fieldName+"' is not selectable in the WebPage");
		} catch (StaleElementReferenceException e) {
			reportStep("Fail","The field '"+fieldName+"' is no longer available in the WebPage");
		} catch (WebDriverException e) {
			reportStep("Fail","Not able to find/interact with The field '"+fieldName);
		}	
		
		return value;
	}

	public void selectVisibileTextById(String id, String value) {
		try {
		WebElement element=driver.findElementById(id);
		Select elementObj=new Select(element);
		elementObj.selectByVisibleText(value);
		reportStep("Pass","The dropdown with id '"+id+"' is found and the value '"+value+"' is selected from the dropdown.");
		}
		catch (NoSuchElementException e) {
			reportStep("Fail","The element with Id '"+id+"' is not found in DOM");
		} catch (ElementNotVisibleException e) {
			reportStep("Fail","The element with Id '"+id+"' is not visible in the WebPage");
		} catch (ElementNotInteractableException e) {
			reportStep("Fail","The element with Id '"+id+"' is not interactable in the WebPage");
		} catch (ElementNotSelectableException e) {
			reportStep("Fail","The element with Id '"+id+"' is not selectable in the WebPage");
		} catch (StaleElementReferenceException e) {
			reportStep("Fail","The element with Id '"+id+"' is no longer available in the WebPage");
		} catch (WebDriverException e) {
			reportStep("Fail","Not able to find/interact with the element with Id '"+id);
		}
	}

	public void selectIndexById(String id, int value) {
	
		try {
			WebElement element=driver.findElementById(id);
			Select elementObj=new Select(element);
			elementObj.selectByIndex(value);
			reportStep("Pass","The dropdown with id '"+id+"' is found and the value at the position'"+value+"' is selected from the dropdown.");
			}
			catch (NoSuchElementException e) {
				reportStep("Fail","The element with Id '"+id+"' is not found in DOM");
			} catch (ElementNotVisibleException e) {
				reportStep("Fail","The element with Id '"+id+"' is not visible in the WebPage");
			} catch (ElementNotInteractableException e) {
				reportStep("Fail","The element with Id '"+id+"' is not interactable in the WebPage");
			} catch (ElementNotSelectableException e) {
				reportStep("Fail","The element with Id '"+id+"' is not selectable in the WebPage");
			} catch (StaleElementReferenceException e) {
				reportStep("Fail","The element with Id '"+id+"' is no longer available in the WebPage");
			} catch (WebDriverException e) {
				reportStep("Fail","Not able to find/interact with the element with Id '"+id);
			}
				
	}

	public void switchToParentWindow() {
		try {
		Set<String> winIDs=driver.getWindowHandles();
		for(String winID:winIDs) {
			driver.switchTo().window(winID);
			break;
		}
		reportStep("Pass","Switched to the last available Window");
		}
		catch (NoSuchWindowException e) {
			reportStep("Fail","The Parent window was not availble");
		}
		catch(NoSuchSessionException e) {
			reportStep("Fail","The session for Parent window is not available");
		}
		catch(WebDriverException e) {
			reportStep("Fail","Problem encountered while switching to the Parent window");
		}
		
	}

	public void switchToLastWindow() {
		
		try {
		Set<String> winIDs=driver.getWindowHandles();
		for(String winID:winIDs) {
			driver.switchTo().window(winID);
		}
		reportStep("Pass","Switched to the last available Window");
		}
		catch (NoSuchWindowException e) {
			reportStep("Fail","The window was not availble");
		}
		catch(NoSuchSessionException e) {
			reportStep("Fail","The session is not available");
		}
		catch(WebDriverException e) {
			reportStep("Fail","Problem encountered while switching to the last window");
		}
			 
		
	}

	public void acceptAlert() {
	
		try {
			driver.switchTo().alert().accept();
			reportStep("Pass","The alert is accepted successfully");
		}catch(NoAlertPresentException e) {
			reportStep("Fail","Alert is not present in the window");
		}
		catch(WebDriverException e) {
			reportStep("Fail","Error occured while accepting the Alert");
		}
		
	}

	public void dismissAlert() {
	  
		try {
			driver.switchTo().alert().dismiss();
			reportStep("Pass","The alert is dismissed successfully");
			
		}catch(NoAlertPresentException e) {
			reportStep("Fail","Alert is not present in the window");
		}
		catch(WebDriverException e) {
			reportStep("Fail","Error occured while dismisisng the Alert");
		}
		
	}

	public String getAlertText() {
		String alertMessage=null;
		
		try {
			alertMessage= driver.switchTo().alert().getText();
			reportStep("Pass","The alert message is '"+alertMessage+"'");
		}catch(NoAlertPresentException e) {
			reportStep("Fail","Alert is not present in the window");
		}
		catch(WebDriverException e) {
			reportStep("Fail","Error occured while switching to the Alert");
		}
		return alertMessage;
	}

	public void takeSnap() {
	 try {
		File tmp= driver.getScreenshotAs(OutputType.FILE);
		 File dest= new File("./screenshots/snap"+i+".png");
		 FileUtils.copyFile(tmp, dest);
	} catch (WebDriverException e) {
	reportStep("Fail","Error while taking screenshot");
	
	} catch (IOException e) {
	reportStep("Fail","Error while creating/copying the image file");
	}
		i++;
	}

	public void closeBrowser() {
	 try { 
	driver.close();	
	reportStep("Pass","The browser is closed successfully");
	 }
	 catch(NoSuchWindowException e) {
		 reportStep("Fail","No window found");
	 }
	 catch(WebDriverException e) {
		 reportStep("Fail","Error while closing the browser");
	 }
	}

	public void closeAllBrowsers() {
		 try { 
				driver.quit();	
				reportStep("Pass","All browsers are closed successfully");
				 }
				 catch(NoSuchWindowException e) {
					 reportStep("Fail","No window/windows found");
				 }
				 catch(WebDriverException e) {
					 reportStep("Fail","Error while closing the browser window/windows");
				 }
	}

	public void enterByIdTab(String idValue, String data) {
		try {
			driver.findElementById(idValue).sendKeys(data,Keys.TAB);
			reportStep("Pass","The value '"+data+"' is entered in the field '"+fieldName with id '"+idValue+"'");
		} catch (NoSuchElementException e) {
			reportStep("Fail","The element with Id '"+idValue+"' is not found in DOM");
		} catch (ElementNotVisibleException e) {
			reportStep("Fail","The element with Id '"+idValue+"' is not visible in the WebPage");
		} catch (ElementNotInteractableException e) {
			reportStep("Fail","The element with Id '"+idValue+"' is not interactable in the WebPage");
		} catch (ElementNotSelectableException e) {
			reportStep("Fail","The element with Id '"+idValue+"' is not selectable in the WebPage");
		} catch (StaleElementReferenceException e) {
			reportStep("Fail","The element with Id '"+idValue+"' is no longer available in the WebPage");
		} catch (WebDriverException e) {
			reportStep("Fail","Not able to find/interact with the element with Id '"+idValue);
		}
		
		
	}

	public void selectIndexByXpath(String xpathValue, int index) {
	try {
		WebElement element=driver.findElement(xpathValue);
		Select obj=new Select(element);
		obj.selectByIndex(index);
		reportStep("Pass","IRCTC Lounge Element' is clicked and the dropdown value at index '"+index+"' is selected");
	}
		catch(NoSuchElementException e) {
			reportStep("Fail","IRCTC Executive lounge Element' not found");
		}
		catch(ElementNotVisibleException e)
		{
			reportStep("Fail","Element' not Visible");
		}
		catch(ElementNotInteractableException e) {
			reportStep("Fail","Element' not interactable");
		}
		catch(ElementNotSelectableException e)
		{
			reportStep("Fail","Element' not Selectable");
		}
		catch(WebDriverException e) {
			reportStep("Fail","Error while clicking on element'");
		}
	

	}

	public void selectVisibleTextByXpath(String xpathValue, String text) {
		try {
		WebElement element=driver.findElement(xpathValue);
		Select obj=new Select(element);
		obj.selectByVisibleText(text);
		reportStep("Pass","Element' is clicked and the dropdown value with value '"+text+"' is selected");
		}
		catch(NoSuchElementException e) {
			reportStep("Fail","Element' not found");
		}
		catch(ElementNotVisibleException e)
		{
			reportStep("Fail","Element' not Visible");
		}
		catch(ElementNotInteractableException e) {
			reportStep("Fail","Element' not interactable");
		}
		catch(ElementNotSelectableException e)
		{
			reportStep("Fail","Element' not Selectable");
		}
		catch(WebDriverException e) {
			reportStep("Fail","Error while clicking on element'");
		}
		 
			 
		
		
	}*/
}
